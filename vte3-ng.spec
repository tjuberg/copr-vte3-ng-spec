%global apiver 2.91

%global fribidi_version 1.0.0
%global glib2_version 2.52.0
%global gnutls_version 3.2.7
%global gtk3_version 3.20.0
%global icu_uc_version 4.8
%global libsystemd_version 220
%global pango_version 1.22.0
%global pcre2_version 10.21


Name:           vte3-ng
Version:        0.59.0
Release:        1%{?dist}
Summary:        Terminal emulator library

License:        LGPLv2+
URL:            https://github.com/thestinger/vte-ng
Source0:        https://github.com/thestinger/vte-ng/archive/%{version}.tar.gz 

	
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  pkgconfig(fribidi) >= %{fribidi_version}
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gnutls) >= %{gnutls_version}
BuildRequires:  pkgconfig(gobject-2.0) >= %{glib2_version}
BuildRequires:  gobject-introspection-devel
BuildRequires:  gperf
BuildRequires:  gtk-doc
BuildRequires:  meson
BuildRequires:  pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires:  pkgconfig(icu-uc) >= %{icu_uc_version}
BuildRequires:  pkgconfig(libpcre2-8) >= %{pcre2_version}
BuildRequires:  pkgconfig(libsystemd) >= %{libsystemd_version}
BuildRequires:  pkgconfig(pango) >= %{pango_version}
BuildRequires:  systemd-rpm-macros
BuildRequires:  vala
 
Requires:       fribidi >= %{fribidi_version}
Requires:       glib2 >= %{glib2_version}
Requires:       gnutls%{?_isa} >= %{gnutls_version}
Requires:       gtk3%{?_isa} >= %{gtk3_version}
Requires:       libicu%{?_isa} >= %{icu_uc_version}
Requires:       pango >= %{pango_version}
Requires:       pcre2%{?_isa} >= %{pcre2_version}
Requires:       systemd >= %{libsystemd_version}
Requires:       vte-profile

Provides: vte291 = %{version}-%{release}
Provides: vte291%{?_isa} = %{version}-%{release}

Conflicts: vte291%{?_isa}

%description
VTE is a library implementing a terminal emulator widget for GTK+. VTE
is mainly used in gnome-terminal, but can also be used to embed a
console/terminal in games, editors, IDEs, etc.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}


%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

# vte-profile is deliberately not noarch to avoid having to obsolete a noarch
# subpackage in the future when we get rid of the vte3 / vte291 split. Yum is
# notoriously bad when handling noarch obsoletes and insists on installing both
# of the multilib packages (i686 + x86_64) as the replacement.
%package -n     vte-profile
Summary:        Profile script for VTE terminal emulator library
License:        GPLv3+
# vte.sh was previously part of the vte3 package
Conflicts:      vte3 < 0.36.1-3

%description -n vte-profile
The vte-profile package contains a profile.d script for the VTE terminal
emulator library.

%prep
%setup -q -n vte-ng-%{version}

%build
# Avoid overriding vte's own -fno-exceptions
# https://gitlab.gnome.org/GNOME/gnome-build-meta/issues/207
%global optflags %(echo %{optflags} | sed 's/-fexceptions //')
 
%meson --buildtype=plain -Ddocs=true
%meson_build

%install
%meson_install

%find_lang vte-%{apiver}
 
%files -f vte-%{apiver}.lang
%license COPYING.GPL3
%doc NEWS
%{_libdir}/libvte-%{apiver}.so.0*
%{_libdir}/girepository-1.0/

%files devel
%{_bindir}/vte-%{apiver}
%{_includedir}/vte-%{apiver}/
%{_libdir}/libvte-%{apiver}.so
%{_libdir}/pkgconfig/vte-%{apiver}.pc
%{_datadir}/gir-1.0/
%doc %{_datadir}/gtk-doc/
%{_datadir}/vala/

%files -n vte-profile
%{_sysconfdir}/profile.d/vte.sh